/*
 * orbita.c
 *
 *  Created on: 06/05/2015
 *      Author: Sergio Filho, Paulo Eduardo, valle
 */

#if __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define ZOOM_STEP 0.05
#define WALK_STEP 0.1
#define FPS 120

typedef struct Planeta {
	double x, y, vx, vy, mass;
} Planeta;

double R(double Cx, double Cy, double x, double y) {
	return sqrt(pow(x - Cx, 2) + pow(y - Cy, 2));
}

double G(double x, double x2, double R, double g) {
	return -g * (x - x2) / pow(R, 3);
}

Planeta passo_sub(Planeta p1, Planeta p2, double h) {
	Planeta p;
	double g = p2.mass;
	p.mass = p1.mass;
	double k1, k2, k3, k4, r1, r2, r12;
	r1 = R(0., 0., p1.x, p1.y);
	r2 = R(0., 0., p2.x, p2.y);
	r12 = R(p2.x, p2.y, p1.x, p1.y);
	k1 = h * (G(p1.x, 0., r1, 1.) + G(p1.x, p2.x, r12, g));
	k2 = h
			* (G(p1.x + k1 / 2, 0., R(0., 0., p1.x + k1 / 2, p1.y), 1.)
					+ G(p1.x + k1 / 2, p2.x, R(p2.x, p2.y, p1.x + k1 / 2, p1.y),
							g));
	k3 = h
			* (G(p1.x + k2 / 2, 0., R(0., 0., p1.x + k2 / 2, p1.y), 1.)
					+ G(p1.x + k2 / 2, p2.x, R(p2.x, p2.y, p1.x + k2 / 2, p1.y),
							g));
	k4 = h
			* (G(p1.x + k3, 0., R(0., 0., p1.x + k3, p1.y), 1.)
					+ G(p1.x + k3, p2.x, R(p2.x, p2.y, p1.x + k3, p1.y), g));
	p.vx = p1.vx + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	k1 = h * (G(p1.y, 0., r1, 1.) + G(p1.y, p2.y, r12, g));
	k2 = h
			* (G(p1.y + k1 / 2, 0., R(0., 0., p1.y + k1 / 2, p1.x), 1.)
					+ G(p1.y + k1 / 2, p2.y, R(p2.y, p2.x, p1.y + k1 / 2, p1.x),
							g));
	k3 = h
			* (G(p1.y + k2 / 2, 0., R(0., 0., p1.y + k2 / 2, p1.x), 1.)
					+ G(p1.y + k2 / 2, p2.y, R(p2.y, p2.x, p1.y + k2 / 2, p1.x),
							g));
	k4 = h
			* (G(p1.y + k3, 0., R(0., 0., p1.y + k3, p1.x), 1.)
					+ G(p1.y + k3, p2.y, R(p2.y, p2.x, p1.y + k3, p1.x), g));
	p.vy = p1.vy + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	k1 = h * p1.vx;
	k2 = h * (p1.vx + k1 / 2);
	k3 = h * (p1.vx + k2 / 2);
	k4 = h * (p1.vx + k3);
	p.x = p1.x + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	k1 = h * p1.vy;
	k2 = h * (p1.vy + k1 / 2);
	k3 = h * (p1.vy + k2 / 2);
	k4 = h * (p1.vy + k3);
	p.y = p1.y + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	return p;
}

void passo(Planeta *p1, Planeta *p2) {
	int i;
	Planeta aux = *p1;
	for (i = 0; i < 4000; i++) {
		*p1 = passo_sub(*p1, *p2, 0.00001);
		*p2 = passo_sub(*p2, aux, 0.00001);
	}
}

GLfloat phi = 0;
GLfloat dist = 0.0;
GLfloat p1x, p1y, p2x, p2y, plx = 0, plz = 0;
Planeta p1, p2;

void init(void) {
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glShadeModel(GL_FLAT);
}

GLfloat ixc = 0.0, iyc = 0.0, dxc = 0.0, dyc = 0.0, axc = 0.0, ayc = 0.0;

void mouseMotion(int x, int y) {
	dxc = axc + (x - ixc);
	dyc = ayc + (y - iyc);
	//printf("position changed, %f, %f\n", dxc, dyc);
}

void mouseClick(int button, int state, int x, int y) {
	//printf("state changed, %d, %d, %d, %d\n", button, state, x, y);
	if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_DOWN)) {
		ixc = (GLfloat) x;
		iyc = (GLfloat) y;
		glutMotionFunc(mouseMotion);
	} else if ((button == GLUT_LEFT_BUTTON) && (state == GLUT_UP)) {
		axc = dxc;
		ayc = dyc;
		glutMotionFunc(NULL);
	}
	if (button == 4)
		dist += ZOOM_STEP;
	else if (button == 3)
		dist -= ZOOM_STEP;
}

void display(void) {
	p1x = p1.x;
	p1y = p1.y;
	p2x = p2.x;
	p2y = p2.y;

	glClear(GL_COLOR_BUFFER_BIT);

	/*parametros gerais*/
	glPushMatrix();
	glTranslatef(plx, 0.0, plz);
	glRotatef(dxc, 0.0, 1.0, 0.0);
	glRotatef(dyc, 1.0, 0.0, 0.0);

	/*Sol*/
	glPushMatrix();
	glTranslatef(0.0, 0.0, 0.0);
	glRotatef(30, 1.0, 0.0, 1.0);
	glRotatef(-phi / 2, 0.0, 0.0, 1.0);
	glColor3f(1.0f, 1.0f, 0.0f);
	glutWireSphere(0.8, 40, 32);
	glPopMatrix();

	/*Planeta p1*/
	glPushMatrix();
	glTranslatef(p1x, p1y, 0.0);
	glRotatef(30, 0.0, 0.0, 1.0);
	glRotatef(phi, 0.0, 0.0, 1.0);
	glColor3f(0.5f, 0.7f, 1.0f);
	glutWireSphere(0.05, 8, 4);
	glPopMatrix();

	/*Planeta p2*/
	glPushMatrix();
	glTranslatef(p2x, p2y, 0.0);
	glRotatef(30, 0.0, 0.0, 1.0);
	glRotatef(phi, 0.0, 0.0, 1.0);
	glColor3f(0.5f, 1.0f, 0.7f);
	glutWireSphere(0.05, 8, 4);
	glPopMatrix();

	glPopMatrix();

	glutSwapBuffers();
}

void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat) w / (GLfloat) h, 1.0, 20.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 5.0 + dist, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 'w':
		plz += WALK_STEP;
		break;
	case 's':
		plz -= WALK_STEP;
		break;
	case 'd':
		plx += WALK_STEP;
		break;
	case 'a':
		plx -= WALK_STEP;
		break;
	case 'z':
		dist += .1;
		break;
	case 'c':
		dist -= .1;
		break;
	default:
		return;
	}
}

/*void idle(void) {
	int i;
	phi -= 2;
	if (phi < -360)
		phi += 720;
	for (i = 0; i < 10000; i++) {
		p1 = passo(p1, p2, 0.00001);
		p2 = passo(p2, p1, 0.00001);
	}
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 5.0 + dist, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glutPostRedisplay();
}*/

void onTimer(int value) {
	int i;
	phi -= 2;
	if (phi < -360)
		phi += 720;
	/*for (i = 0; i < 2000; i++) {
		p1 = passo(p1, p2, 0.00001);
		p2 = passo(p2, p1, 0.00001);
	}*/
	passo(&p1, &p2);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0, 0.0, 5.0 + dist, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glutTimerFunc(1000 / FPS, onTimer, 1);
	glutPostRedisplay();
}

int main(int argc, char** argv) {
	/*planeta.x = 3;
	 planeta.y = 0;
	 planeta.vx = 0.0;
	 planeta.vy = 0.6;*/
	if (argc < 11) {
		printf("USAGE: %s x1 y1 vx1 xy1 mass1 x2 y2 vx2 xy2 mass2\n", argv[0]);
		return EXIT_FAILURE;
	}
	p1.x = atof(argv[1]);
	p1.y = atof(argv[2]);
	p1.vx = atof(argv[3]);
	p1.vy = atof(argv[4]);
	p1.mass = atof(argv[5]);
	p2.x = atof(argv[6]);
	p2.y = atof(argv[7]);
	p2.vx = atof(argv[8]);
	p2.vy = atof(argv[9]);
	p2.mass = atof(argv[10]);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowSize(500, 500);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);

	init();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	//glutIdleFunc(idle);
	glutTimerFunc(1000 / FPS, onTimer, 1);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouseClick);

	glutMainLoop();

	return EXIT_SUCCESS;
}
