#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct Planeta {
	double x, y, vx, vy;
} Planeta;

double R(double Cx, double Cy, double x, double y) {
	return sqrt(pow(x - Cx, 2) + pow(y - Cy, 2));
}

double G(double x, double x2, double R, double g) {
	return -g * (x - x2) / pow(R, 3);
}

Planeta Passo(Planeta p1, Planeta p2, double g, double h) {
	Planeta p;
	double k1, k2, k3, k4, r1, r2, r12;
	r1 = R(0., 0., p1.x, p1.y);
	r2 = R(0., 0., p2.x, p2.y);
	r12 = R(p2.x, p2.y, p1.x, p1.y);
	k1 = h * (G(p1.x, 0., r1, 1.) + G(p1.x, p2.x, r12, g));
	k2 = h
			* (G(p1.x + k1 / 2, 0., R(0., 0., p1.x + k1 / 2, p1.y), 1.)
					+ G(p1.x + k1 / 2, p2.x, R(p2.x, p2.y, p1.x + k1 / 2, p1.y),
							g));
	k3 = h
			* (G(p1.x + k2 / 2, 0., R(0., 0., p1.x + k2 / 2, p1.y), 1.)
					+ G(p1.x + k2 / 2, p2.x, R(p2.x, p2.y, p1.x + k2 / 2, p1.y),
							g));
	k4 = h
			* (G(p1.x + k3, 0., R(0., 0., p1.x + k3, p1.y), 1.)
					+ G(p1.x + k3, p2.x, R(p2.x, p2.y, p1.x + k3, p1.y), g));
	p.vx = p1.vx + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	k1 = h * (G(p1.y, 0., r1, 1.) + G(p1.y, p2.y, r12, g));
	k2 = h
			* (G(p1.y + k1 / 2, 0., R(0., 0., p1.y + k1 / 2, p1.x), 1.)
					+ G(p1.y + k1 / 2, p2.y, R(p2.y, p2.x, p1.y + k1 / 2, p1.x),
							g));
	k3 = h
			* (G(p1.y + k2 / 2, 0., R(0., 0., p1.y + k2 / 2, p1.x), 1.)
					+ G(p1.y + k2 / 2, p2.y, R(p2.y, p2.x, p1.y + k2 / 2, p1.x),
							g));
	k4 = h
			* (G(p1.y + k3, 0., R(0., 0., p1.y + k3, p1.x), 1.)
					+ G(p1.y + k3, p2.y, R(p2.y, p2.x, p1.y + k3, p1.x), g));
	p.vy = p1.vy + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	k1 = h * p1.vx;
	k2 = h * (p1.vx + k1 / 2);
	k3 = h * (p1.vx + k2 / 2);
	k4 = h * (p1.vx + k3);
	p.x = p1.x + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	k1 = h * p1.vy;
	k2 = h * (p1.vy + k1 / 2);
	k3 = h * (p1.vy + k2 / 2);
	k4 = h * (p1.vy + k3);
	p.y = p1.y + (k1 + 2 * k2 + 2 * k3 + k4) / 6;
	return p;
}

int main() {
	int i, j, q, M[600][600], i0, cont = 0;
	double m1, m2, h = 0.0001;
	char fname[128], fname1[128];
	Planeta p1, p2;
	FILE *arq;
	printf("Sistema Sol-Planeta-Planeta 1.0\n");
	printf("Digite o nome do arquivo GIF de saida: ");
	scanf("%s", fname1);
	printf("Tempo do gif (s):");
	scanf("%d", &q);
	printf("Condições iniciais do primeiro planeta:\n");
	printf("Vx: ");
	scanf("%lf", &p1.vx);
	printf("Vy: ");
	scanf("%lf", &p1.vy);
	p1.x = 0.;
	p1.y = 1.;
	printf("Condições iniciais do segundo planeta:\n");
	printf("Rx: ");
	scanf("%lf", &p2.x);
	printf("Ry: ");
	scanf("%lf", &p2.y);
	printf("Vx: ");
	scanf("%lf", &p2.vx);
	printf("Vy: ");
	scanf("%lf", &p2.vy);
	printf("Massas (do Sol é 1):\n");
	printf("Planeta 1:");
	scanf("%lf", &m1);
	printf("Planeta 2:");
	scanf("%lf", &m2);
	for (i = 0; i < 600; i++)
		for (j = 0; j < 600; j++) {
			if (fabs(i - 300) > 5 || fabs(j - 300) > 5)
				M[i][j] = 1;
			else
				M[i][j] = 0;
		}
	for (i0 = 0; i0 < 20 * q; i0++) {
		for (i = 0; i < 500; i++) {
			p1 = Passo(p1, p2, m2, h);
			p2 = Passo(p2, p1, m1, h);
		}
		if (i0 < 10)
			sprintf(fname, "%s000%d.pgm", fname1, i0);
		else {
			if (i0 < 100)
				sprintf(fname, "%s00%d.pgm", fname1, i0);
			else {
				if (i0 < 1000)
					sprintf(fname, "%s0%d.pgm", fname1, i0);
				else {
					sprintf(fname, "%s%d.pgm", fname1, i0);
				}
			}
		}
		arq = fopen(fname, "w");
		fprintf(arq, "P2 600 600 1 ");
		for (i = 0; i < 600; i++)
			for (j = 0; j < 600; j++) {
				if ((fabs(i - 300 + 100 * p1.y) < 2
						&& fabs(j - 300 - 100 * p1.x) < 2)
						|| (fabs(i - 300 + 100 * p2.y) < 3
								&& fabs(j - 300 - 100 * p2.x) < 3))
					M[i][j] = 0;
				fprintf(arq, "%d ", M[i][j]);
			}
		fclose(arq);
	}
	sprintf(fname, "convert -delay 10 *.pgm %s.gif", fname1);
	system(fname);
	system("rm -f *.pgm");
	sprintf(fname, "eog %s.gif", fname1);
	system(fname);
}

